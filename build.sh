#!/bin/bash

# Locate SDK up to two folders before this and import pico_sdk_import.cmake
if [[ "${PICO_SDK_PATH}" == "" ]]; then
    echo "Unable to locate pico-sdk, aborting!" 1>&2
    exit 1
fi

# Scrub build folder and build inside of it
if [[ -d "build" ]]; then rm -rf "build"; fi
mkdir build && cd build
cmake .. && make -j$(cat /proc/cpuinfo | grep bogomips | wc -l)

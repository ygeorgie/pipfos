/**
 * @file drv_rtcc.h
 *
 * @brief Real-Time Clock header file
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 27/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */
#ifndef _DRV_RTCC_H_INCLUDE_
#define _DRV_RTCC_H_INCLUDE_

#include <stdint.h>
#include <stdbool.h>
#include <sys/time.h>

#define UPTIME_STR_SZ 8

/* number of seconds since the Epoch, 1970-01-01 00:00:00 +0000 (UTC) */
extern time_t kernel_sys_time;

/* kernel uptime in seconds */
extern uint32_t kernel_uptime;

void     drvrtcc_init(bool clkreset);
uint64_t drvrtcc_set_sys_time(void);
char*    drvrtcc_uptime(uint32_t secuptime);

#endif /* _DRV_RTCC_H_INCLUDE_ */

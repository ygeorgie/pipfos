/**
 * @file drv_rtcc.c
 *
 * @brie fReal-Time Clock driver
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 27/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */

#include <stdio.h>
#include <string.h>

#include <FreeRTOS.h>
#include <task.h>
#include "drv/drv_rtcc.h"
#include <pico/time.h>

time_t kernel_sys_time = 0; /* kernel time in seconds relative to the Epoch, 1970-01-01 00:00:00 +0000 (UTC) */
uint32_t kernel_uptime = 0; /* kernel uptime in seconds */

repeating_timer_t timer;


static bool timer_callback(repeating_timer_t *rt)
{
        kernel_uptime++;
        return true; /* keep repeating */
}


/* fire-up RTCC alarm to trigger once a second */
static void configure_rtcc_alarm(void)
{
        /* negative timeout means _exact_ delay (rather than delay between callbacks) */
        if (!add_repeating_timer_us(-1000000, timer_callback, NULL, &timer)) {
                printf("Failed to add timer\n");
                return;
        }
}


/**
  * @brief Initializes timing module
  *
  * @param clkreset -- resets system clock if true
  *
  * Should be called once immediately after power-up.
  *
  * @return true -- if successful
  */
void drvrtcc_init(bool clkreset)
{
        configure_rtcc_alarm();

        (void) drvrtcc_set_sys_time(); /* set initial ses_sys_time */
}


/**
 * @brief Read the time from the RTCC register && update our kernel time var 'ses_sys_time'
 *
 * @return time since EPOCH in ms
 */
uint64_t drvrtcc_set_sys_time(void)
{
        return 0;
}


/* give uptime in HRF (aka Human-Readable Form) */
char* drvrtcc_uptime(uint32_t secuptime)
{
        static char kupt[UPTIME_STR_SZ]; /* Kernel uptime HRF string */
        uint32_t days;
        uint32_t hrs;
        uint32_t min = secuptime/60;
        uint32_t sec = secuptime%60;

        memset(kupt, 0, UPTIME_STR_SZ);

        if (min < 60) {
                /* less then an hour [mm.ss] */
                snprintf(kupt, UPTIME_STR_SZ, "%dm%ds", min, sec);
        } else if (min < 24*60) {
                /* less then a day [hh.mm] */
                secuptime = min; /* total is in minutes */
                hrs = secuptime/60;
                min = secuptime%60;
                snprintf(kupt, UPTIME_STR_SZ, "%dh%dm", hrs, min);
        } else {
                /* more then a day. [dd.hh] */
                hrs = secuptime/(60*60); /* drop min here */
                secuptime  = hrs; /* total is in hours */
                days = secuptime/24;
                hrs  = secuptime%24;
                snprintf(kupt, UPTIME_STR_SZ, "%dd%dh", days, hrs);
        }

        return kupt;
}

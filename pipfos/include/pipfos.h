/**
 * @file pipfos.h
 *
 * @brief Pico PI FreeRTOS
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 26/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */
#ifndef _PIPFOS_H_INCLUDE_
#define _PIPFOS_H_INCLUDE_

#define KERN_FWV_MAJ 0
#define KERN_FWV_MIN 2

#define KERN_VERS_STR "PipFos smp"

/* UART config */
#define UART_ID   uart0
#define BAUD_RATE 460800


#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#endif  /* _PIPFOS_H_INCLUDE_ */

/**
 * @file main.c
 *
 * @brief our entry point
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 23/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */

#include <pico/stdlib.h>
#include <FreeRTOS.h>
#include <task.h>

#include "tasks/task_init.h"
#include "pipfos.h"

#include "drv/drv_rtcc.h"

int main(void)
{
        /* init pipi stuff */
        stdio_init_all();

        (void)uart_set_baudrate(UART_ID, BAUD_RATE);

        drvrtcc_init(true);

        /* Light'Em Up, Boys! */
        pipfos_start();

        /* RTOS scheduler kicks-in && never returns */
        vTaskStartScheduler();

        /* not reachable... or is it? */
        while (1)
                configASSERT(0);
}

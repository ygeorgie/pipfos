/**
 * @file libfs.h
 *
 * @brief pipi file system. FATFS32 it is...
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 27/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */
#ifndef _LIBFS_H_INCLUDE_
#define _LIBFS_H_INCLUDE_

#include <stdbool.h>

void libfs_ls(void);
bool libfs_rm(char *fnm);

#endif  /* _LIBFS_H_INCLUDE_ */

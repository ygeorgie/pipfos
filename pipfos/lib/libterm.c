/**
 * @file libterm.c
 *
 * @brief our astonishing bash
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 27/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */

#include <string.h>

#include <FreeRTOS.h>
#include <task.h>

#include "pipfos.h"
#include "lib/libterm.h"
#include "lib/libfs.h"
#include "drv/drv_rtcc.h"

#define TERMINAL_ECHO_ON true   /* set to 'false' to disable echo */

#define CONSOLE_PROMPT "\r\npish:> "
#define CLI_BSZ 512
#define TEMP_TEXT_BSZ 196

/* pipi-bash (aka pish) top level cmds */
#define CMD_BASH_HELP1  "help"
#define CMD_BASH_HELP2  "?"
#define CMD_BASH_UPTIME "uptime"
#define CMD_BASH_LS     "ls"
#define CMD_BASH_RM     "rm"


typedef void(*cvp)(char*); /* Command Vector Pointer */

typedef struct {
        const char *cmdId;  /* pish cmd */
        cvp         cmdVec; /* pish command callback vector */
} pishCmd_t;

typedef enum {
        LIB_UART_INIT,
        LIB_UART_AUTH_USR,
        LIB_UART_AUTH_PWD,
        LIB_UART_READY,
        LIB_UART_PROCESSING,
        LIB_UART_ERROR
} tsh_sms_t;

static tsh_sms_t tshsms = LIB_UART_INIT; /* state machine state */

/* command text buffer */
static char ctb[TEMP_TEXT_BSZ];

/* CLI buffer */
static char     tshCmdBuf[CLI_BSZ] = { [0 ... CLI_BSZ-1] = 0 };
static uint32_t tshCmdIdx;


static void cmdBash(char *cmd);
static void tabCmd(void); /* magic [TAB] */


/* all possible top level TSH commands */
static const pishCmd_t cmdLst[] = {
        { CMD_BASH_HELP1,  cmdBash },
        { CMD_BASH_HELP2,  cmdBash },
        { CMD_BASH_UPTIME, cmdBash },
        { CMD_BASH_LS,     cmdBash },
        { CMD_BASH_RM,     cmdBash },
};


/**
 * @brief Handles top level shell commands
 *
 * @param cmd -- command line
 */
static void cmdBash(char *cmd)
{
        int i;

        (void) sscanf(cmd, "%s", ctb); /* read command */

        /* act as a maaaan */
        if (!strncmp(ctb, CMD_BASH_LS, strlen(CMD_BASH_LS))) {
                libfs_ls();
        } else if (!strncmp(ctb, CMD_BASH_RM, strlen(CMD_BASH_RM))) {
                /* skip command string and goto parameter */
                cmd += strlen(CMD_BASH_RM) + 1;
                if (!libfs_rm(cmd))
                        printf("rm: [%s] no such file\n\r", cmd);
        } else if (!strncmp(ctb, CMD_BASH_UPTIME, strlen(CMD_BASH_UPTIME))) { /* uptime */
                printf("\n\rTotal: [%d]sec\n", kernel_uptime);
                printf("%s\n", drvrtcc_uptime(kernel_uptime));
        } else { /* list all supported/possible top commands */
                printf("Avail cmds:\n");
                for (i = 0; i < ARRAY_SIZE(cmdLst); i++) {
                        printf("[%02d] %s\n", i, cmdLst[i].cmdId);
                }
        }
}

/**
 * @brief Sends a character to the terminal if boEchoChar is true.
 *
 * @param cChar      -- Character to send
 * @param boEchoChar -- If true the character will be sent to the terminal
 */
static inline void char2Term(char cChar, bool doecho)
{
        if (doecho)
                uart_putc_raw(UART_ID, cChar);
}


static void tabCmd(void)
{
        uint32_t i;
        uint32_t cmdMatch = 0;
        uint32_t cmdIdx = 0;

        for (i = 0; i < ARRAY_SIZE(cmdLst); i++) {
                if (memcmp(cmdLst[i].cmdId, tshCmdBuf, tshCmdIdx) == 0) {
                        cmdMatch++;
                        cmdIdx = i;
                }
        }

        if (cmdMatch == 1) {
                uart_puts(UART_ID, cmdLst[cmdIdx].cmdId + tshCmdIdx);
                while (cmdLst[cmdIdx].cmdId[tshCmdIdx] != '\0') {
                        tshCmdBuf[tshCmdIdx] = cmdLst[cmdIdx].cmdId[tshCmdIdx];
                        tshCmdIdx++;
                }
        }
}


static inline void resetCLIbuf(void)
{
        tshCmdIdx = 0;
}


static uint32_t searchCmd(const char *cmd)
{
        uint32_t i;

        for (i = 0; i < ARRAY_SIZE(cmdLst); i++)
                if (strncmp(cmdLst[i].cmdId, cmd, strlen(cmdLst[i].cmdId)) == 0)
                        return i;

        return i;
}


/**
 * @brief Process a new line
 *
 * @param newLine -- line to be processed
 *
 * <long-description>
 *
 * @return returns false if no more CMD prompt shall be provided
 */
static bool processLine(char *newLine)
{
        uint32_t cmdIdx;

        if (newLine[0] == '\0')
                return true;

        cmdIdx = searchCmd(newLine);

        if (cmdIdx < ARRAY_SIZE(cmdLst)) {
                if (cmdLst[cmdIdx].cmdVec != NULL)
                        cmdLst[cmdIdx].cmdVec(newLine);

                if (cmdLst[cmdIdx].cmdVec == lterm_rst)
                        return false;
        } else {
                uart_puts(UART_ID, "\r\nUnknown cmd\n");
        }

        return true;
}


/**
 * @brief Processes a new received character
 *
 * Adds it to the buffer line if there is enough space.
 * Returns the whole line if CR.
 *
 * @param newChar -- new char to add to the line buffer
 * @param doEcho  -- if true -- character will be echoed on the terminal
 *
 * @return the whole line if CR
 */
static char* manageLine(char newChar, bool doEcho)
{
        if (tshCmdIdx < (sizeof(tshCmdBuf) - 1)) {
                switch (newChar) {
                case 127: /* Backspace */
                        if (tshCmdIdx > 0) {
                                tshCmdIdx--;
                                char2Term(newChar, doEcho);
                        }
                        break;

                case '\t':
                        if (tshCmdIdx > 0)
                                tabCmd();
                        break;

                case '\n':
                case '\r':
                        tshCmdBuf[tshCmdIdx] = 0;
                        char2Term(newChar, doEcho);
                        return tshCmdBuf;

                default:
                        if (tshCmdIdx < (sizeof(tshCmdBuf) - 1)) {
                                tshCmdBuf[tshCmdIdx] = newChar;
                                tshCmdIdx++;
                                char2Term(newChar, doEcho);
                        }
                        break;
                }
        } else {
                tshCmdIdx = 0; /* reset line buffer if buffer is full */
        }

        return NULL;
}


/**
 * @brief Receive new char from UART
 *
 * <long-description>
 *
 * @return new received char
 */
static char readChar(void)
{
        uint8_t ch = 0;

        if (uart_is_readable(UART_ID))
                ch = uart_getc(UART_ID);

        return (char)ch;
}


void lterm_rst(char *cmd)
{
	tshsms = LIB_UART_INIT;
}


/* pipfos bash task Manager */
void lterm_task(void)
{
        char stdIn = 0;
        char *lbuf;

        switch (tshsms) {

        case LIB_UART_INIT:
                stdIn = uart_getc(UART_ID); /* clean garbage */
                uart_puts(UART_ID, CONSOLE_PROMPT); /* put prompt */
                tshsms = LIB_UART_READY;
                break;

        case LIB_UART_READY:
                stdIn = readChar(); /* read a char from terminal */
                if (stdIn) { /* new char received */
                        lbuf = manageLine(stdIn, TERMINAL_ECHO_ON); /* manage line buffer */
                        if (lbuf) { /* line available (ENTER pressed) */
                                if (processLine(lbuf)) {
                                        uart_puts(UART_ID, CONSOLE_PROMPT);
                                }
                                resetCLIbuf();
                        }
                }
                break;

        default:
                tshsms = LIB_UART_INIT;
    }

}

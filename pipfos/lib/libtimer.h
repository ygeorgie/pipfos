/**
 * @file libtimer.h
 *
 * @brief Timer library header
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 26/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */
#ifndef _LIB_TIMER_H_INCLUDE_
#define _LIB_TIMER_H_INCLUDE_

#include <stdint.h>
#include <stdbool.h>

uint32_t lib_tim_sys_tick_get(void);
void     lib_tim_to_reset(uint32_t *to);
bool     lib_tim_to_expired(uint32_t tref, uint32_t to);

#endif	/* _LIB_TIMER_H_INCLUDE_ */

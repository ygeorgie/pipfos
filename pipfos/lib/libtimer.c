/**
 * @file libtimer.c
 *
 * @brief Timer library
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 26/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */

#include <FreeRTOS.h>
#include <task.h>
#include "lib/libtimer.h"


/**
 * @brief   Returns the current system tick count
 *
 * @return  Return system tick count.
 */
uint32_t lib_tim_sys_tick_get(void)
{
        return xTaskGetTickCount();
}


/**
 * @brief   returns the current system tick count
 * @param   obj -- timeout object to reset; must be a valid pointer!
 */
void lib_tim_to_reset(uint32_t *obj)
{
        /* Set object to current sys tick */
        *obj = xTaskGetTickCount();
}


/**
 * @brief Computes the difference between a timestamp and the current TickCount.
 *
 * It takes into account the timeout period of the given timestamp too.
 *
 * @param reft   -- reference timestamp
 * @param period -- timeout period of @reft timestamp
 *
 * @remarks The caller of this function can safely cast the returned value to
 * a smaller type (e.g. int32_t) if he is absolutely sure the expected value
 * fits into it. For example when @ref period counts seconds or minutes even
 * hours and there is no billions of milliseconds differences between
 * @reft and now
 *
 * @warning This function must be called after @reft was initialized by
 * calling xTaskGetTickCount()
 *
 * @return Difference in ticks on a signed 64 bits integer
 */
static int64_t lib_tim_sys_tick_delta(uint32_t reft, uint32_t period)
{
        uint32_t now = xTaskGetTickCount();

        /* compute the diff between now and the reference time; it is computed
         * on a uint32 in order to get the correct difference, no matter which
         * value is greater */
        uint32_t time_delta = now - reft;

        /* we have to return a signed64 bit number, due to the worst case
         * scenario the difference is in range [-4294967295‬ .. 4294967295‬] as
         * follow:
         *   - when now = 0xFFFFFFFF and reference = period = 0:
         *	return value is: 4,294,967,295‬
         *   - when now = reference and period = 0xFFFFFFFF:
         *	return value is: -4,294,967,295‬ */

        return (int64_t)time_delta - period;
}


/**
 * @brief Check if timeout has expired
 *
 * @param tref -- reference time stamp to be check
 *                this parameter should be set by
 *                xTaskGetTickCount() or lib_tim_sys_tick_get()
 * @param to   -- timeout in ms
 *
 * Due to the tick function, @tref and @period are all stored on
 * unsigned 32bit integers, it is taken into account that these values
 * are limited to store a period of maximum 49.7 days.
 * Due to the function always considers the @tref is in the
 * past, it must be initialized with the xTaskGetTickCount() to ensure it gets
 * a valid value. In the case of xTaskGetTickCount() is less than @tref,
 * it means the tick is overflowed and the function will do the correct
 * computation.
 *
 * @return  true if timeout has expired, false otherwise
 */
bool lib_tim_to_expired(uint32_t tref, uint32_t to)
{
        return lib_tim_sys_tick_delta(tref, to) >= 0;
}

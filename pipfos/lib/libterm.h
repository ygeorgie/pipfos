/**
 * @file libterm.h
 *
 * @brief pipfos CLI. (aka pish)
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 27/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */
#ifndef _LIB_TERM_H_INCLUDE_
#define _LIB_TERM_H_INCLUDE_

#include <stdbool.h>
#include <stdio.h>
#include <pico/stdlib.h>
#include <hardware/uart.h>

void lterm_task(void);
void lterm_rst(char *cmd);

#endif  /* _LIB_TERM_H_INCLUDE_ */

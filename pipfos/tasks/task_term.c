/**
 * @file task_term.c
 *
 * @brief UART terminal
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 22/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */

#include <stdio.h>
#include <stdint.h>

#include <pico/stdlib.h>
#include <FreeRTOS.h>
#include <task.h>

#include "tasks/task_term.h"
#include "tasks/task_init.h"
#include "lib/libterm.h"

static const piptask_t *tdescr = NULL;


/**
 * @brief UART bootstrap
 *
 * @return bla-bla
 */
bool term_init(void *param)
{
        /* baud rate is already set to 460800 in main() */
        return true;
}


void task_term(void *param)
{
        uint32_t delay;

        tdescr = pipfos_get_task(param);
        delay = tdescr->tDelay;

        /* main task loop */
        while (true) {
                lterm_task();
                vTaskDelay(delay);
        }
}

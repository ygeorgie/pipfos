/**
 * @file task_usr.h
 *
 * @brief
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 23/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */
#ifndef _TASK_USER_H_INCLUDE_
#define _TASK_USER_H_INCLUDE_

bool tusr_init(void *param);
void task_user(void *param);

#endif  /* _TASK_USER_H_INCLUDE_ */

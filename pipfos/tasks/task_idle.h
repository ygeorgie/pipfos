/**
 * @file task_idle.h
 *
 * @brief
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 23/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */
#ifndef _TASK_IDLE_H_INCLUDE_
#define _TASK_IDLE_H_INCLUDE_

void task_idle(void *param);

#endif  /* _TASK_IDLE_H_INCLUDE_ */

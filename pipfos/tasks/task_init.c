/**
 * @file task_init.c
 *
 * @brief All tasks are described here.
 *
 * Add new task description in this file
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 22/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */

#include <stdio.h>
#include <string.h>

#include "tasks/task_init.h"
#include "pipfos.h"

#include "tasks/task_idle.h"
#include "tasks/task_term.h"
#include "tasks/task_usr.h"

#include "lib/libtimer.h"

/* idle task params */
#define TIDLE_STACK_SZ configMINIMAL_STACK_SIZE /* Stack size in WORDS, NOT bytes! */
#define TIDLE_PRIO     tskIDLE_PRIORITY         /* 0 */
#define TIDLE_DELAY    900                      /* in ms */

/* tty task params */
#define TTTY_STACK_SZ (8*configMINIMAL_STACK_SIZE) /* Stack size in WORDS, NOT bytes! */
#define TTTY_PRIO     (tskIDLE_PRIORITY+2)
#define TTTY_DELAY    20                       /* ms */

/* usr task params */
#define TUSR_STACK_SZ configMINIMAL_STACK_SIZE /* Stack size in WORDS, NOT bytes! */
#define TUSR_PRIO     (tskIDLE_PRIORITY+2)
#define TUSR_DELAY    5                        /* ms */

static piptask_t pipTasks[] = {
        [0] = { NULL,      task_idle, "idle", TIDLE_STACK_SZ, TIDLE_PRIO, TIDLE_DELAY, NULL }, /* kernel idle task */
        [1] = { term_init, task_term, "tty",  TTTY_STACK_SZ,  TTTY_PRIO,  TTTY_DELAY,  NULL }, /* our eyes */
        [2] = { tusr_init, task_user, "usr",  TUSR_STACK_SZ,  TUSR_PRIO,  TUSR_DELAY,  NULL }, /* user-defined one */
        /* any new task should be described here */
};


static void startTask(piptask_t *pstT)
{
	portBASE_TYPE rc;
	TaskHandle_t xHandle;

	rc = xTaskCreate(pstT->tVector, (const char*)pstT->tName, pstT->tStackSz, pstT->tName, pstT->tPrio, &xHandle);

	if (rc != pdPASS)
		printf("Task [%s] can't be created (rc %d)\n", pstT->tName, rc);

	pstT->tHndl = xHandle;  /* save newly created task handle */
}


/* say hello or something... */
static void bootstrap_msg(void)
{
        printf("\r\n\r\n\r\n\r\n");
        printf("################################################################################");
        printf("\r\n");
        printf("%s v[%02u.%02u]\n", KERN_VERS_STR, KERN_FWV_MAJ, KERN_FWV_MIN);
        printf("\r\n\r\n");
}


void pipfos_start(void)
{
        int i;

        bootstrap_msg(); /* say hello */

        for (i = 0; i < ARRAY_SIZE(pipTasks); i++) {
                if (pipTasks[i].tInit == NULL)
                        continue;

                if (!pipTasks[i].tInit(pipTasks[i].tName)) {
                        printf("task [%s] init ERROR\n", pipTasks[i].tName);
                        /* TODO: should we return here? */
                        return;
                }
        }

        for (i = 0; i < ARRAY_SIZE(pipTasks); i++)
                startTask(&pipTasks[i]);
}


/* get task descr */
piptask_t* pipfos_get_task(char *tname)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(pipTasks); i++)
		if (!strcmp(pipTasks[i].tName, tname))
			return &pipTasks[i];

	return NULL;
}


/**
 * @brief Initialize a subtask object
 *
 * @param delay -- object to be updated. If this parameter is NULL or
 *                 invalid the behavior is undefined
 *
 * @param period The interval the subtask will run
 */
void subtask_init(struct task_delay *delay, uint32_t period)
{
        memset(delay, 0, sizeof(*delay));
        delay->period = period;
        subtask_update(delay);
}


/**
 * @brief Updates task last running time
 *
 * @param delay -- object to be updated. If this parameter is NULL or invalid
 *                 the behavior is undefined
 */
void subtask_update(struct task_delay *delay)
{
        delay->last_run = lib_tim_sys_tick_get();
}


/**
 * @brief Tests whether a subtask is expired
 *
 * @param delay -- object to be checked. If this parameter is NULL or invalid
 *                 the behavior is undefined
 *
 * @return @true if subtask is expired
 */
bool subtask_expired(const struct task_delay *delay)
{
        return lib_tim_to_expired(delay->last_run, delay->period);
}

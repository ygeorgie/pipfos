/**
 * @file task_usr.c
 *
 * @brief user code is here
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 23/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */

#include <stdio.h>
#include <stdint.h>

#include <pico/stdlib.h>
#include <FreeRTOS.h>
#include <task.h>

#include "tasks/task_usr.h"
#include "tasks/task_init.h"

static const piptask_t *tdescr = NULL;


bool tusr_init(void *param)
{
        /* do whatever is needed before task is created */
        return true;
}


void task_user(void *param)
{
        uint32_t delay;
        struct task_delay normal_delay;
        uint32_t cntr = 0;

        tdescr = pipfos_get_task(param);
        delay = tdescr->tDelay;

        subtask_init(&normal_delay, delay * 200); /* 1sec */

        /* main task loop */
        while (true) {

                 /* Normal priority subtasks */
                if (subtask_expired(&normal_delay)) {
                        //printf("Hello, PiPFOs [%d]\n", cntr++);
                        subtask_update(&normal_delay);
                }


                vTaskDelay(delay);
        }
}

/**
 * @file task_idle.c
 *
 * @brief
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 22/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */

#include <stdio.h>
#include <stdint.h>

#include <pico/stdlib.h>
#include <FreeRTOS.h>
#include <task.h>

#include "tasks/task_idle.h"
#include "tasks/task_init.h"

static const piptask_t *tdescr = NULL;

#define IDLE_DELAY_STEPS     36
#define IDLE_DELAY_UNITS_MS (tdescr->tDelay/IDLE_DELAY_STEPS)

void task_idle(void *param)
{
        uint32_t task_delay;
        uint toggle;
        const uint LED_PIN = 25;

        gpio_init(LED_PIN);
        gpio_set_dir(LED_PIN, GPIO_OUT);

        tdescr = pipfos_get_task(param);

        while (1) {
                /* kernel heartbit */
                toggle = 1;
                gpio_put(LED_PIN, toggle);

                /* this 'for' loop is equivalent to vTaskDelay(900) but calls IDLE_DELAY_STEPS times the profiling routine */
                for (task_delay = 0; task_delay < IDLE_DELAY_STEPS; task_delay++) {
                        vTaskDelay(IDLE_DELAY_UNITS_MS);
                }

                toggle ^= 1;
                gpio_put(LED_PIN, toggle);

                vTaskDelay(100);
        }
}

/**
 * @file task_init.h
 *
 * @brief
 *
 * <long description>
 *
 * @author Copyright (C) 2021-2022 HWTeam. ataraxic <cixarata@ataraxic.org>
 *
 * @date Created on 23/02/2021
 *
 * @contributors
 *
 * @section license_sec License
 *          GPLv2
 */
#ifndef _TASK_INIT_H_INCLUDE_
#define _TASK_INIT_H_INCLUDE_

#include <stdint.h>

#include <pico/stdlib.h>
#include <FreeRTOS.h>
#include <task.h>

/**
 * @brief Task initialization function
 *
 * @return @true upon success, @false otherwise
 */
typedef bool(*tinit_t)(void*);

/* task descriptor */
typedef struct {
        tinit_t         tInit;    /* trampoline */
        TaskFunction_t  tVector;  /* task state machine */
        char           *tName;    /* task name */
        uint16_t        tStackSz; /* task stack */
        uint32_t        tPrio;    /* task priority */
        TickType_t      tDelay;   /* task delay */
        TaskHandle_t    tHndl;    /* will be set after task is created */
} piptask_t;

void pipfos_start(void);
piptask_t* pipfos_get_task(char *tname);


/**
 * @brief Mechanism that allows a sub-task to be executed at a specified interval
 */
struct task_delay {
        uint32_t last_run;  /* last running time (in System Tick Count) */
        uint32_t period;    /* (sub)task running interval */
};

void subtask_init(struct task_delay *delay, uint32_t period);
void subtask_update(struct task_delay *delay);
bool subtask_expired(const struct task_delay *delay);

#endif  /* _TASK_INIT_H_INCLUDE_ */

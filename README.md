# Pi PICO Pi (aka PiPi.. isn't that amaizing!) running FreeRTOS.

rp2040 is scalable enough to have some kernel onboard.  
There are some, popping up here && there ([FUZIX](https://www.raspberrypi.org/blog/how-to-get-started-with-fuzix-on-raspberry-pi-pico/) for ex.)  
I am using good-old [FreeRTOS](https://www.freertos.org/Documentation/RTOS_book.html) SMP branch

## Disclamer
All the developmen is (and will be) done using *sane* OS, which is far from
being M$ / MacOS...  
There is no intention whatsoever to have any support for them.

## Building
After cloning -- first thing todo, is to fetch submodules:  
* git submodule update --init

In order to build an image -- you should set correctly PICO_SDK_PATH to point
to a *pico-sdk* submodule:
* cd ./pico-sdk
* export PICO_SDK_PATH=$(pwd)  

Along with FREERTOS_KERNEL_PATH to point to *FreeRTOS* submodule:
* cd ./frt
* export FREERTOS_KERNEL_PATH=$(pwd)  

First build goes happily using *./build.sh* helper.  
Afterwards you can goto *./build* directory && do make from there.  
Executable is ./build/pipfos.uf2. Just boot your *pico2040* in bootmode and copy newly compiled *.uf2* image over to the target.

## Eyes on the target
### Get FTDI Serial TTL-232 cable:
![FTDI](docs/images/ttl-232-USB.cable.png)

### hook Rx/Tx/GND to the target UART0
![UART0](docs/images/pipi-uart0.png)

| TTL-232 | pipi |
| --- | --- |
| Tx  -- Yellow wire | pin#1 |
| Rx  -- Orange wire | pin#2 |
| GND -- Black wire  | pin#3 |

### Here is how it can look like:
![LAYOUT1](docs/images/pipi-serial.png)

or

![LAYOUT2](docs/images/pipi-cytron.png)

### Use good-old minicom with the following .minirc config file:
cat ~/.minirc.USB0  
  
pu port             /dev/ttyUSB0  
pu baudrate         460800  
pu mhangup  
pu backspace        BS  
pu hasdcd           No  
pu rtscts           No  
pu linewrap         Yes  

### cli run it:
minicom USB0 -C minicom.capturefile.USB0

## avoid USB plug/unplug to deliver binaries
Connect push-switch to pins [38]GND and [37]3V3_EN  
and use it together with *bootsel* button to put *pipi* in a USB storage mode:  
  
![RESET-BUTTON](docs/images/pipi-rst.button.png)


Or.. you can just buy something like this:  
  
![USB-CABLE](docs/images/pipi-usb-cable.png)

and live happily ever-after (-;  
  
* As soon as you will have eyes on the target -- you can use minimalistic
build-in Pico Pi bash (aka _pish_).  
Type '?' for avail commands.

## SDC wiring
I've used [this](https://www.sparkfun.com/products/544) microSD Breakout  
and hook it to SPI0 interface using pins 21 ~ 25:
| MicroSD | pipi | SPI0
| --- | --- | -- |
| CD  | not connected |  |
| DO  | pin#21 | Rx  |
| GND | pin#23 |     |
| SCK | pin#24 | SCK |
| VCC | pin#36 |     |
| DI  | pin#25 | Tx  |
| CS  | pin#22 | CSn |

Note, that i did not propagate Card Detected INT pin, as SDC is expected to be always present.

### Here is how it looks like:
![LAYOUT3](docs/images/pipi-sdc.png)

## Actual development layout
For the sake of easiness [Cytron](docs/images/pipi-cytron.png) breakout board will be used for further *pipfos* development, as we do have everything
we need already routed out for us (including ESP-01 and SDC)

## Next steps
* Support ESP-01 WiFi piggyback
* Support SDC with FFS (aka FAT FileSystem)
* SDC encryption
* Feather-lightweight SSH server

## Acknowledgements
I would like to thank PicoCPP for [RPI-pico-FreeRTOS](https://github.com/PicoCPP/RPI-pico-FreeRTOS), which was my starting point for this project.  
Another reference project i was using is [FreeRTOS-FAT-CLI-for-RPi-Pico](https://github.com/carlk3/FreeRTOS-FAT-CLI-for-RPi-Pico)  
Thx guys!

## License
[GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)
